import abc
from enum import Enum
import logging
from os import environ
from typing import Any, cast
import requests
import json

from building import BuiltMod, Loader
from config import BASE_PATH, CURSEFORGE_ID, MODRINTH_ID

logger = logging.getLogger("ModPublisher")


class Target(Enum):
    CurseForge = "curseforge"
    Modrinth = "modrinth"


class Publisher(abc.ABC):
    @abc.abstractmethod
    def publish(self, mod: BuiltMod, dry_run: bool):
        """Publish a built mod"""

    def get_changelog(self, mod: BuiltMod) -> str:
        path = BASE_PATH / "changelogs" / mod.properties.modversion
        if not path.exists():
            logger.error(f"Could not find file {path}")
            raise RuntimeError(
                f"No changelog for version {mod.properties.modversion} could be found"
            )
        return path.read_text()


class CurseforgePublisher(Publisher):
    def __init__(self) -> None:
        super().__init__()
        self.token = environ["CURSEFORGE_TOKEN"]
        self.cf_api_version_types = cast(
            list[Any],
            requests.get(
                "https://minecraft.curseforge.com/api/game/version-types",
                params={"token": self.token},
            ).json(),
        )
        self.cf_api_versions = cast(
            list[Any],
            requests.get(
                "https://minecraft.curseforge.com/api/game/versions",
                params={"token": self.token},
            ).json(),
        )

        json.dump(self.cf_api_versions, open("versions.json", "w"))
        json.dump(self.cf_api_version_types, open("version_types.json", "w"))

    def find_version_type(self, name: str) -> int:
        version_type = next(
            filter(
                lambda vt: vt["name"] == name,
                self.cf_api_version_types,
            ),
            None
        )
        if version_type is None:
            raise ValueError(f"Version Type {name} not found")
        logger.info(f"Version type {name} = {version_type['id']}")
        return version_type["id"]

    def find_version(self, name: str, typ: int) -> int:
        version = next(
            filter(
                lambda vt: vt["name"] == name and vt["gameVersionTypeID"] == typ,
                self.cf_api_versions,
            )
        )
        logger.info(f"Version {typ}: {name} = {version['id']}")
        return version["id"]

    def find_version_by_slug(self, slug: str, typ: int) -> int:
        version = next(
            filter(
                lambda vt: vt["slug"] == slug and vt["gameVersionTypeID"] == typ,
                self.cf_api_versions,
            )
        )
        logger.info(f"Version {typ}: {slug} = {version['id']}")
        return version["id"]

    # Versions in the Curseforge API are (IMO) majorly broken
    # There are multiple versions under api/game/version with the same name and
    # the only way to differentiate them is by their gameVersionTypeID
    # This wouldn't be so bad if that ID was unique for say Forge, Mincraft, Fabric, etc.
    # But EACH major Minecraft version (1.20, 1.19, etc.) has its own TypeID.
    # So we have to find that first and then select the correct type
    def get_mc_version_type_id(self, mc_version: str) -> int:
        if mc_version.count(".") > 1:
            major_mc_version = mc_version.rsplit(".", 1)[0]
        else:
            major_mc_version = mc_version
        version_type_id = self.find_version_type(f"Minecraft {major_mc_version}")
        v = self.find_version(mc_version, version_type_id)
        logger.info(f"MC {mc_version} = {v}")
        return v

    def get_loader_type_id(self, loader: Loader) -> int:
        modloader_type = self.find_version_type("Modloader")
        return self.find_version_by_slug(loader.value, modloader_type)

    def get_java_version_id(self, java_v: int) -> int:
        java_type = self.find_version_type("Java")
        return self.find_version(f'Java {java_v}', java_type)

    def publish(self, mod: BuiltMod, dry_run: bool):
        versions = json.loads(mod.properties.compatible_versions)
        logger.info(f"compatible_versions = {versions}")
        versions = [self.get_mc_version_type_id(v) for v in versions]
        loader = self.get_loader_type_id(mod.loader)
        versions.append(loader)
        versions += map(self.get_java_version_id, [21])

        data = {
            "changelog": self.get_changelog(mod),
            "changelogType": "markdown",
            "gameVersions": versions,
            "releaseType": "release",
        }
        if not dry_run:
            res = requests.post(
                f"https://minecraft.curseforge.com/api/projects/{CURSEFORGE_ID}/upload-file",
                data={"metadata": json.dumps(data)},
                files={"file": mod.jarfile.open("rb")},
                headers={"X-API-Token": self.token},
            )

            if res.status_code == 200:
                logger.info("Published successfully, id=%s", json.loads(res.content)["id"])
            else:
                logger.error("Publishing faild with error: %s, because: %s", res.status_code, res.reason)
        else:
            logger.info("Publishing to Curseforge in DryRun mode")
            logger.info("curseforge json data")
            logger.info(data)
            logger.info("File: %s", mod.jarfile)
            if not mod.jarfile.exists():
                logger.warn("File does not exist!")


class ModrinthPublisher(Publisher):
    def __init__(self) -> None:
        super().__init__()
        self.token = environ["MODRINTH_TOKEN"]

    def publish(self, mod: BuiltMod, dry_run: bool):
        compatible_versions = json.loads(mod.properties.compatible_versions)
        version = mod.properties.modversion
        loader = mod.loader.name
        name = mod.properties.name
        modrinth_body = {
            "name": f"{name} {loader} {version}",
            "version_number": version,
            "changelog": self.get_changelog(mod),
            "dependencies": [],
            "game_versions": compatible_versions,
            "version_type": "release",
            "loaders": [mod.loader.value],
            "featured": True,
            "project_id": MODRINTH_ID,
            "file_parts": ["file"],
        }
        modrinth_data = json.dumps(modrinth_body)
        headers = {
            "User-Agent": f"456Xander/{mod.properties.modid}/{mod.properties.modversion} (alexander.daum@mailbox.org)",
            "Authorization": self.token,
        }
        if not dry_run:
            res = requests.post(
                "https://api.modrinth.com/v2/version",
                data={"data": modrinth_data},
                files={"file": mod.jarfile.open("rb")},
                headers=headers,
            )
            if res.status_code == 200:
                logger.info("Published successfully")
            else:
                logger.error("Publishing faild with error: %s, because %s", res.status_code, res.reason)
                logger.info(res.content)
        else:
            logger.info("Publishing to Modrinth in DryRun mode")
            logger.info("Modrinth json data")
            logger.info(modrinth_data)
            logger.info("Modrinth Headers")
            logger.info(headers)
            logger.info("File: %s", mod.jarfile)
            if not mod.jarfile.exists():
                logger.warn("File does not exist!")


__publishers: dict[Target, Publisher] = {}


def get_publisher(target: Target):
    if not target in __publishers:
        match target:
            case Target.CurseForge:
                pub = CurseforgePublisher()
            case Target.Modrinth:
                pub = ModrinthPublisher()
        __publishers[target] = pub
    return __publishers[target]
