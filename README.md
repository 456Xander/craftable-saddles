## Craftable Saddles 

This mod adds crafting recipes for saddles and horse armor.

Get the mod here on [curseforge](https://www.curseforge.com/minecraft/mc-mods/craftable-saddles)
