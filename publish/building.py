from dataclasses import dataclass
from enum import Enum
from pathlib import Path
import subprocess
import logging

logger = logging.getLogger("ModPublisher")


class Loader(Enum):
    Forge = "forge"
    # NeoForge = "neoforge"
    # Fabric = "fabric"


class GradleConfig:
    def __init__(self, loader: Loader, path: Path) -> None:
        gradle_cfg = subprocess.check_output(
            ["./gradlew", "--console=plain", "--quiet", "properties"],
            cwd=path,
        )
        self.loader = loader
        self.props: dict[str, str] = {}
        for l in gradle_cfg.decode().splitlines():
            parts = l.split(":", 1)
            if len(parts) == 2:
                self.props[parts[0].strip()] = parts[1].strip()

    def jar_name(self) -> str:
        mod_id = self.modid
        mc_version = self.props["mc_version"]
        mod_version = self.props["mod_version"]
        return f"{mod_id}-{mc_version}-{mod_version}.jar"

    @property
    def modid(self) -> str:
        return self.props["mod_id"]

    @property
    def name(self) -> str:
        return self.props["mod_name"]

    @property
    def modversion(self) -> str:
        return self.props["mod_version"]

    @property
    def mcversion(self) -> str:
        return self.props["mc_version"]

    @property
    def compatible_versions(self) -> str:
        return self.props["compatible_mc_versions"]


@dataclass
class BuiltMod:
    properties: GradleConfig
    loader: Loader
    jarfile: Path


def build_mod(loader: Loader) -> BuiltMod:
    logger.info(f"Building mod for {loader}")
    path = Path(__file__).parents[1]  # the main dir
    subprocess.run(
        ["./gradlew", "--console=plain", "build"],
        cwd=path,
        stdout=subprocess.DEVNULL,
    )
    props = GradleConfig(loader, path)
    jarfile = path / "build" / "libs" / props.jar_name()
    return BuiltMod(props, loader, jarfile)
